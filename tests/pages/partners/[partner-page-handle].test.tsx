/** @jest-environment jsdom */
import { render } from '@testing-library/react';

describe('Partner page', () => {
  const originalMatchMedia = global.matchMedia;

  beforeAll(() => {
    global.matchMedia = (): any => [];
  });

  afterAll(() => {
    global.matchMedia = originalMatchMedia;
  });

  it('should load without crashing', async () => {
    const PartnerPage = (await import('../../../pages/partners/[partner-page-handle]')).default;

    expect(() => {
      render(<PartnerPage componentBlocks={[]} />);
    }).not.toThrowError();
  });
});
