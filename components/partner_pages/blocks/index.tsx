/* eslint-disable react/no-array-index-key */
import CallToActionBlock, { BlockProps as CallToActionBlockProps } from './CallToAction';
import TestimonialBlock, { BlockProps as TestimonialBlockProps } from './Testimonial';
import UseCaseBlock, { BlockProps as UseCaseBlockProps } from './UseCase';
import HeroBlock, { BlockProps as HeroBlockProps } from './Hero';
import AdvantagesBlock, { BlockProps as AdvantagesBlockProps } from './Advantages';
import OffersBlock, { BlockProps as OffersBlockProps } from './Offers';
import BuyingBlock, { BlockProps as BuyingBlockProps } from './Buying';

import { PartnerPageBlockType } from '../../../types/partnerPages';

import Schemas from './schemas';

export const componentContainer = {
  [PartnerPageBlockType.HERO]: HeroBlock,
  [PartnerPageBlockType.TESTIMONIAL]: TestimonialBlock,
  [PartnerPageBlockType.USE_CASE]: UseCaseBlock,
  [PartnerPageBlockType.CALL_TO_ACTION]: CallToActionBlock,
  [PartnerPageBlockType.ADVANTAGES]: AdvantagesBlock,
  [PartnerPageBlockType.OFFERS]: OffersBlock,
  [PartnerPageBlockType.BUYING]: BuyingBlock,
};

export const jsonSchemaMappings = {
  [PartnerPageBlockType.HERO]: Schemas.HERO,
  [PartnerPageBlockType.PROCESS]: Schemas.PROCESS,
  [PartnerPageBlockType.TESTIMONIAL]: Schemas.TESTIMONIAL,
  [PartnerPageBlockType.PRICING]: Schemas.PRICING,
  [PartnerPageBlockType.FAQ]: Schemas.FAQ,
  [PartnerPageBlockType.GALLERY]: Schemas.GALLERY,
  [PartnerPageBlockType.USE_CASE]: Schemas.USE_CASE,
  [PartnerPageBlockType.CALL_TO_ACTION]: Schemas.CTA,
  [PartnerPageBlockType.VIDEO]: Schemas.VIDEO,
  [PartnerPageBlockType.ADVANTAGES]: Schemas.ADVANTAGES,
  [PartnerPageBlockType.OFFERS]: Schemas.OFFERS,
  [PartnerPageBlockType.REGISTRATION]: Schemas.REGISTRATION,
  [PartnerPageBlockType.BUYING]: Schemas.BUYING,
  [PartnerPageBlockType.BUYING_SKU]: Schemas.BUYING_SKU,
  [PartnerPageBlockType.CUSTOM_HTML]: Schemas.CUSTOM_HTML,
  [PartnerPageBlockType.CARESHIP_WIDGET]: Schemas.CARESHIP_WIDGET,
};

export type Data = HeroBlockProps
| AdvantagesBlockProps
| CallToActionBlockProps
| TestimonialBlockProps
| UseCaseBlockProps
| OffersBlockProps
| BuyingBlockProps;
