import React from 'react';
import styled from 'styled-components';

type HeaderAnchorOffsetProps = React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> & {
  id: string;
}

/**
 * This component compensate the header that is floating when we redirect user to anchor link (so they can see the content)
 * The id prop is mandatory because anchor makes no sense without id!
 */
export const HeaderAnchorOffset = styled.div<HeaderAnchorOffsetProps>`
  &:before {
    display: block;
    content: " ";
    margin-top: -160px;
    height: 160px;
    visibility: hidden;
  }
  @media only screen and (max-width: 768px) {
    &:before {
      margin-top: -80px;
      height: 80px;
    }
  }
`;
