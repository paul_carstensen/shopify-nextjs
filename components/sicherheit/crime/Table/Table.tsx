import React from 'react';
import styled from 'styled-components';
import { CrimeStats } from '../../../../types/sicherheit/wie-sicher-ist-dein-wohnort';

const Table = styled.table`
  margin: auto;
  width: 100%;
  color: white;
  border-collapse:collapse;
  background: none;
`;

const THead = styled.thead`
  background-color: #7f909e;
`;

const TBody = styled.tbody`
`;

const TR = styled.tr`
  background-color: transparent;
  color: #19232d;
  font-weight: 500;
  &:nth-child(odd){
    background-color: #c4d6e6;
  }
  &:nth-child(even){
    background-color: #ffffff;
  }
  &:last-child{
    td:first-child{
      border-bottom-left-radius: 25px;
    }
    td:last-child{
      border-bottom-right-radius: 25px;
    }
  }
  @media screen and (max-width: 768px) {
    &:last-child{
      td:first-child{
        border-bottom-left-radius: 0px;
      }
      td:last-child{
        border-bottom-right-radius: 0px;
      }
    }
  }
`;

const TH = styled.th`
  background-color: transparent;
  font-size: 11px;
  padding: 8px 15px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  text-align: left;
  &.big-font {
    font-size: 16px;
  }
  &:first-child{
    border-top-left-radius: 25px;
  }
  &:last-child{
    border-top-right-radius: 25px;
  }
  @media screen and (max-width: 768px) {
    padding: 5px;
    &:first-child{
      border-top-left-radius: 0px;
    }
    &:last-child{
      border-top-right-radius: 0px;
    }
  }
`;

const TD = styled.td`
  background-color: transparent;
  font-size: 11px;
  padding: 8px 15px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  word-break: break-word;
  &.big-font {
    font-size: 11px;
  }
  span {
    text-transform: uppercase;
    &.hoch {
      color: #db1111;
    }
    &.mittel {
      color: #e87e04;
    }
  }
  @media screen and (max-width: 768px) {
    padding: 5px 4px;
  }
`;

export interface TableProps {
  resultData: CrimeStats[];
}

export const ResultTable: React.FC<TableProps> = ({ resultData }) => (
  <Table>
    <THead>
      <TH className="big-font">Ausgewählte Verbrechensarten</TH>
      <TH>Anzahl Taten</TH>
      <TH>Relative Anzahl</TH>
      <TH>Aufklärungs-quote</TH>
    </THead>
    <TBody>
      {
        resultData.map(({ crimeType, numCases, relativeRisk, clearancePercentage }) => (
          <TR key={crimeType}>
            <TD className="big-font">{crimeType}</TD>
            <TD>{numCases.toLocaleString('de-DE')}</TD>
            <TD><span className={relativeRisk.toLowerCase()}>{relativeRisk}</span></TD>
            <TD>{clearancePercentage}%</TD>
          </TR>
        ))
      }
    </TBody>
  </Table>
);
