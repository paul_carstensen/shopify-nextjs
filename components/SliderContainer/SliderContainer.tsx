import React from 'react';
import styled from 'styled-components';
import watermark from '../../assets/images/sustainability/watermark-slide.png';
import watermarkDarker from '../../assets/images/sustainability/watermark-slide-darker.png';

const SliderContainerInner = styled.div<{ backgroundColor?: string, watermarkBg?: boolean }>`
  min-height: calc(100vh - 178px);
  scroll-snap-align: start;
  ${(props) => `background: ${props.backgroundColor}`};
  ${(props) => {
    if (props.watermarkBg && props.backgroundColor === '#ffffff') {
      return `background-image: url(${watermarkDarker.src})`;
    } if (props.watermarkBg) {
      return `background-image: url(${watermark.src})`;
    }
    return null;
  }};
  background-position-x: right;
  background-repeat: no-repeat;
  background-size: 60%;

  & h1 {
    margin: 0 0 7.5px;
    font-family: "Quicksand", sans-serif;
    font-weight: 400;
    letter-spacing: 0em;
    line-height: 1;
    text-transform: uppercase;
    font-size: 2em;
    @media only screen and (min-width: 769px) {
      margin: 0 0 15px;
    }
  }

  & h2 {
    margin: 0 0 7.5px;
    font-family: "Quicksand", sans-serif;
    font-weight: 400;
    letter-spacing: 0em;
    line-height: 1;
    text-transform: uppercase;
    font-size: 1.72em;
    @media only screen and (min-width: 769px) {
      margin: 0 0 15px;
    }
  }

  & div.slider-container {
    min-height: calc(100vh - 178px);
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: 4% 10%;
    
    & h1, h2, h3 {
      line-height: normal
    };

    & .slider-container-main h1.title {
      font-size: 46px;
      text-transform: none;
      font-weight: bold;
      padding-bottom: 35px;
    }
  
    & .slider-container-main h2.subtitle {
      font-size: 22px;
      text-transform: none;
      font-weight: 600;
    }
  
    & .slider-container-main p.text {
      font-size: 18px;
    }

    & .slider-container-head  {
      text-align: right;
    }

    & .slider-container-head span {
      font-size:46px;
      font-weight: bold;
    }
  }

  @media screen and (max-width: 768px) {
    width: 100%;
    background-size: 90%;
    background-position-x: 40vw;
    min-height: calc(100vh - 178px);
    height: 100%;

    & div.slider-container {
      min-height: calc(100vh - 178px);
      padding: 25px;
      max-width: inherit;

      & .slider-container-main h1.title {
        font-size:26px;
      }

      & .slider-container-main h2.subtitle {
        font-size: 16px;
      }

      & .slider-container-main p.text {
        font-size: 14px;
      }

      & .slider-container-head span {
        font-size:26px;
      }
    }
  }
`;

export interface SliderContainerProps {
  counter?: string;
  numSlides?: string;
  title?: string;
  subtitle?: string;
  text?: string;
  backgroundColor?: string;
  watermarkBg?: boolean;
  children?: React.ReactNode;
  id?: string;
}

export const SliderContainer: React.FC<SliderContainerProps> = ({
  children,
  title,
  subtitle,
  text,
  backgroundColor,
  watermarkBg,
  counter,
  numSlides,
}: SliderContainerProps) => (
  <SliderContainerInner id={counter ? `tipp-slide-${counter}` : undefined} backgroundColor={backgroundColor} watermarkBg={watermarkBg}>
    <div className="slider-container">
      <div className="slider-container-head">
        {counter && numSlides ? <span className="count">{counter}/{numSlides}</span> : false}
      </div>

      <div className="slider-container-main">
        <h1 className="title">{title}</h1>
        <h2 className="subtitle">{subtitle}</h2>
        <p className="text">{text}</p>

        {children}
      </div>

      <div className="slider-container-footer" />
    </div>
  </SliderContainerInner>
);
