import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import styles from './ErrorPage.module.css';
import ProductHighlights from '../products/ProductHighlights';
import { PageWidthContainer } from '../common/PageWidthContainer';

type ErrorPageProps = {
  errorText: string;
  bestsellers?: any[];
  bestsellersCollectionHandle?: string;
}

const PageWidthHeaderWidth = styled(PageWidthContainer)`
  @media only screen and (max-width: 959px) {
    padding: 0px;
  }
`;

const ErrorPage: React.FC<ErrorPageProps> = ({ errorText, bestsellers, bestsellersCollectionHandle }) => {
  const bestsellersComponent = bestsellersCollectionHandle && bestsellers && bestsellers.length > 0
    ? <ProductHighlights collectionHandle={bestsellersCollectionHandle} products={bestsellers} /> : null;

  return (
    <>
      <div className={styles.header}>
        <PageWidthHeaderWidth>
          <div>
            <img
              className={styles.image}
              alt="Fehler beim laden der Seite Header Bild"
              src="//cdn.shopify.com/s/files/1/0268/4819/8771/files/fatty-corgi.png?v=1613720985"
            />
          </div>
        </PageWidthHeaderWidth>
      </div>
      <PageWidthContainer>
        <div className={styles.headline}>
          OH NEIN...<br />
          {errorText}
        </div>
        <div className={styles.subheadline}>
          Gehe zur&nbsp;
          <Link href="/">
            <a>Startseite</a>
          </Link>
        </div>
      </PageWidthContainer>
      {bestsellersComponent}
    </>
  );
};

export default ErrorPage;
