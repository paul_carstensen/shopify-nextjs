import React from 'react';
import styled from 'styled-components';

import { Lifestyle } from '../../../../types/risk/lifestyle';
import { Tab } from '../Tab';

type TabsProps = {
  lifestyles: Lifestyle[];
  onChange: (index: number) => void;
  selectedTabKey: number;
}

const TabsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  margin: 22px 0;
`;

export const Tabs: React.FC<TabsProps> = ({ lifestyles, selectedTabKey, onChange }) => (
  <TabsWrapper>
    {lifestyles.map((lifestyle, index) => (
      <Tab
        key={lifestyle.lifestyleKey}
        firstTab={index === 0}
        lastTab={index === lifestyles.length - 1}
        item={lifestyle}
        selected={selectedTabKey === index}
        onClick={() => onChange(index)}
      />
    ))}
  </TabsWrapper>
);
