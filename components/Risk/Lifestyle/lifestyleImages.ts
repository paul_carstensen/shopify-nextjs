import { Lifestyle } from 'types/risk/lifestyle';
import FreedomMind from '../../../assets/images/lifestyles/freedom_mind.webp';
import GateKeeper from '../../../assets/images/lifestyles/gatekeeper.webp';
import HipAndHopper from '../../../assets/images/lifestyles/hip_and_hopper.webp';
import LeHipster from '../../../assets/images/lifestyles/le_hipster.webp';
import LuxuryLover from '../../../assets/images/lifestyles/luxury_lover.webp';
import MindMaker from '../../../assets/images/lifestyles/mind_maker.webp';
import Multitasker from '../../../assets/images/lifestyles/multitasker.webp';
import SilverAthlete from '../../../assets/images/lifestyles/silver_athlete.webp';
import SocialPolitican from '../../../assets/images/lifestyles/social_politican.webp';
import SoloDj from '../../../assets/images/lifestyles/solo_dj.webp';
import Storyteller from '../../../assets/images/lifestyles/storyteller.webp';
import SunHerdsman from '../../../assets/images/lifestyles/sun_herdsman.webp';
import UrbanGuy from '../../../assets/images/lifestyles/urban_guy.webp';
import Visionary from '../../../assets/images/lifestyles/visionary.webp';
import WorldPilot from '../../../assets/images/lifestyles/world_pilot.webp';
import YogiGuru from '../../../assets/images/lifestyles/yogi_guru.webp';
import Autopilot from '../../../assets/images/lifestyles/autopilot.webp';
import OnlineAvatar from '../../../assets/images/lifestyles/online_avatar.webp';

const images: Record<Lifestyle['lifestyleKey'], StaticImageData> = {
  freedom_mind: FreedomMind,
  gatekeeper: GateKeeper,
  hip_and_hopper: HipAndHopper,
  le_hipster: LeHipster,
  luxury_lover: LuxuryLover,
  mind_maker: MindMaker,
  multitasker: Multitasker,
  silver_athlete: SilverAthlete,
  social_politican: SocialPolitican,
  solo_dj: SoloDj,
  storyteller: Storyteller,
  sun_herdsman: SunHerdsman,
  urban_guy: UrbanGuy,
  visionary: Visionary,
  world_pilot: WorldPilot,
  yogi_guru: YogiGuru,
  autopilot: Autopilot,
  online_avatar: OnlineAvatar,
};

export default images;
