import React, { useState } from 'react';
import styled from 'styled-components';
import { Button, Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import { Lifestyle } from '../../../../types/risk/lifestyle';

type TabBodyProps = {
  lifestyle: Lifestyle | undefined
}

const TabBodyWrapper = styled.div`
  margin: 12px 0;
`;

const CoreGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const CoreGroupAmountWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  margin-right: 12px;
  font-weight: bold;
`;

const CoreGroupAmount = styled.span`
  font-size: 40px;
  line-height: 40px;
  margin-right: 8px;
`;

const CoreGroupAgeText = styled.span`
  font-size: 12px;
  font-weight: 600;
  font-family: QuickSand;
`;

const SliderItemTitle = styled.span`
  font-size: 14px;
  font-weight: bold;
`;

const SliderItemDescription = styled.span`
  font-size: 12px;
  font-family: Quicksand;
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  padding: 38px 20px;
  box-shadow: 5px 10px 30px 0 rgba(178, 178, 178, 0.25);
`;

const ExplanantionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 25px;
  margin-bottom: 18px;
  padding-top: 25px;
`;

const ExplanationTitle = styled.span`
  font-size: 14px;
  font-weight: bold;
  text-align: left;
`;

const ExplanationDescription = styled.span`
  font-size: 12px;
  font-family: Quicksand;
  text-align: left;
`;

const ShowMore = styled.span`
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;
  font-family: Poppins;
`;

export const TabBody: React.FC<TabBodyProps> = ({ lifestyle }) => {
  const [showMore, setShowMore] = useState(false);
  if (!lifestyle) {
    return null;
  }

  return (
    <TabBodyWrapper>
      <CoreGroup>
        <CoreGroupAmountWrapper>
          <CoreGroupAmount>{`${lifestyle.kpiCoregroupAmount}`.replace('.', ',')}</CoreGroupAmount>
          <span>Mio.</span>
        </CoreGroupAmountWrapper>
        <CoreGroupAgeText>in Deutschland<br />({lifestyle.kpiCoregroupAge} Jahr alt)</CoreGroupAgeText>
      </CoreGroup>
      <Slider
        visibleSlidesBreakpoints={[
          { breakpoint: '(max-width: 560px)', value: 1 },
          { breakpoint: '(min-width: 561px)', value: 1 },
        ]}
        enableArrows
        enableMouseWheel
      >
        <Card>
          <SliderItemTitle>Grundüberzeugung</SliderItemTitle>
          <SliderItemDescription>{lifestyle.statementBeliefs}</SliderItemDescription>
        </Card>
        <Card>
          <SliderItemTitle>Grundüberzeugung</SliderItemTitle>
          <SliderItemDescription>{lifestyle.statementMotto}</SliderItemDescription>
        </Card>
        <Card>
          <SliderItemTitle>Grundüberzeugung</SliderItemTitle>
          <SliderItemDescription>{lifestyle.statementLifegoal}</SliderItemDescription>
        </Card>
      </Slider>
      <ExplanantionWrapper>
        <ExplanationTitle>Lifestyle Explanation:</ExplanationTitle>
        <ExplanationDescription>{showMore ? lifestyle.description : lifestyle.shortDescription} <ShowMore onClick={() => setShowMore(!showMore)}>{showMore ? 'Weniger' : 'Mehr'}</ShowMore></ExplanationDescription>
      </ExplanantionWrapper>
      <Button appearance="primary">Yes, it is me!</Button>
    </TabBodyWrapper>
  );
};
