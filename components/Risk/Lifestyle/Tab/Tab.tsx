import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import lifestyleImages from '../lifestyleImages';

import { Lifestyle } from '../../../../types/risk/lifestyle';

type TabProps = {
  onClick: () => void;
  item: Lifestyle;
  selected: boolean;
  firstTab: boolean;
  lastTab: boolean;
}

type TabWrapperProps = {
  selected: boolean;
  firstTab: boolean;
  lastTab: boolean;
}

const TabWrapper = styled.div<TabWrapperProps>`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: fit-content;
  padding: 8px;
  border-top-left-radius: ${(props) => (props.firstTab || props.selected ? '8px' : '0')};
  border-top-right-radius: ${(props) => (props.lastTab || props.selected ? '8px' : '0')};
  background-color: ${(props) => (props.selected ? '#19232d' : '#e6e6e6')};
  color: ${(props) => (props.selected ? 'white' : '#19232d')};
  cursor: pointer;
`;

const TabTitle = styled.span`
  text-align: center;
  font-family: Poppins;
  font-size: 12px;
  margin-top: 8px;
  font-weight: bold
`;

export const Tab: React.FC<TabProps> = ({ item, firstTab, lastTab, selected, onClick }) => (
  <TabWrapper firstTab={firstTab} lastTab={lastTab} selected={selected} onClick={onClick}>
    <Image
      src={lifestyleImages[item.lifestyleKey]}
      width={selected ? 60 : 40}
      height={selected ? 60 : 40}
      alt={item.title}
    />
    <TabTitle>{item.title}</TabTitle>
  </TabWrapper>
);
