import { CategoryKey, InterestKey, categoryKeys, interestKeys } from 'helpers/homePageConfig';

const localStorageKey = 'home_page_user_preferences';

export type UserPreference = {
  categories: CategoryKey[];
  interest: InterestKey;
};

export const isUserPreferenceKnown = (up?: Partial<UserPreference>): up is UserPreference =>
  Boolean(up && up.categories && up.categories.length > 0 && up.interest); // eslint-disable-line implicit-arrow-linebreak

export const loadUserPreferences = (): UserPreference | undefined => {
  try {
    const json = JSON.parse(localStorage.getItem(localStorageKey) ?? '');
    if (!Array.isArray(json?.categories)) return undefined;
    if (!json.categories.every((item: CategoryKey) => categoryKeys.includes(item))) return undefined;
    if (!(typeof json?.interest === 'string')) return undefined;
    if (!interestKeys.includes(json?.interest)) return undefined;
    return json as UserPreference;
  } catch {
    return undefined;
  }
};

export const saveUserPreferences = (userPreferences: UserPreference) => {
  localStorage.setItem(localStorageKey, JSON.stringify(userPreferences));
};
