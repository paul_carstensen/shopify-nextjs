import React from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import imageUrl from '../../../../assets/images/xmas-gimmick/demo-image.png';

const ResultImageContent = styled.div`
  padding-bottom: 30px;
  display: flex;
  flex-direction: column;
`;

const ImageContainer = styled.div`
  position: relative;
  width: 420px;
  height: 380px;
  align-self: center;
  
  div {
    border-radius: 26px;
  }

  @media screen and (max-width: 768px) {
    width: 336px;
    height: 300px;
  }  
`;

const Title = styled.div`
  padding-top: 18px;
  font-family: Poppins;
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  line-height: 1.43;
  text-align: center;
  padding-bottom: 15px;
  text-transform: uppercase;
  width: 75%;
  margin: auto;
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

const SubTitle = styled.div`
  width: 75%;
  margin: auto;
  font-family: Poppins;
  font-size: 22px;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  font-weight: normal;
  line-height: 1.43;
  text-align: center;
  padding-bottom: 15px;
  text-transform: uppercase;
  @media screen and (max-width: 768px) {
    width: 100%;
    font-size: 16px;
  }
`;

export interface ResultImageComponentProps {
  title: string;
  subTitle: string;
  image?: string;
}

export const ResultImageComponent: React.FC<ResultImageComponentProps> = ({ title, subTitle, image }) => (
  <ResultImageContent>
    <Title>{title}</Title>
    <SubTitle>{subTitle}</SubTitle>
    <ImageContainer>
      <Image
        src={image ? `data:image/jpeg;base64,${image}` : imageUrl}
        alt="Ergebnis"
        layout="fill"
        objectFit="cover"
        objectPosition="center"
        data-tracking-id="ga-weihnachtselfen-calendar-image"
      />
    </ImageContainer>
  </ResultImageContent>
);
