import React from 'react';
import styled from 'styled-components';

export interface XmasGimmickHeroProps {
  title: string;
  backgroundImage: string;
  backgroundImageMob: string;
  contentAlign?: 'left' | 'center' | 'right';
}

const HeroContainer = styled.div<{ backgroundImage: string, backgroundImageMob: string }>`
  text-align: center;
  ${(props) => `background-image: url(${props.backgroundImage});`}
  background-size: cover;
  background-position: center;
  position: relative;
  height: 399px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 26px;
  @media screen and (max-width: 768px) {
    height: 300px;
    ${(props) => `background-image: url(${props.backgroundImageMob});`}
  }
`;

const HeadTitle = styled.h1`
  font-family: Poppins;
  font-size: 46px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #fff;

  @media screen and (max-width: 768px) {
    font-size: 24px;
  }
`;

export const XmasGimmickHero: React.FC<XmasGimmickHeroProps> = ({ title, backgroundImage, backgroundImageMob }) => (
  <HeroContainer
    title={title}
    backgroundImage={backgroundImage}
    backgroundImageMob={backgroundImageMob}
  >
    <HeadTitle className={title}>{title}</HeadTitle>
  </HeroContainer>
);
