import React, { useEffect, useState, useMemo } from 'react';
import styled from 'styled-components';
import lottie, { AnimationConfigWithData, AnimationItem } from 'lottie-web';
import snowMan from '../../../../assets/animations/snowman.json';

const LoadingWrapper = styled.div`
  border-radius: inherit;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 250px;
  background-color: #19232d;
  margin-bottom: 20px;
  border-radius: 26px;
`;

const AnimationContainer = styled.div`
  height: 150px;
`;

const Text = styled.p`
  margin-top: -30px;
  color: white;
`;

type LottieSettings = Omit<AnimationConfigWithData, 'container'>;

const useAnimation = ({
  containerId,
  lottieSettings,
}: {
  containerId: string;
  lottieSettings: LottieSettings;
}) => {
  const [animation, setAnimation] = useState<AnimationItem | null>(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const cachedSettings = useMemo(() => lottieSettings, []);

  useEffect(() => {
    const container = document.getElementById(containerId);
    let animationInstance: AnimationItem | null = null;

    if (container) {
      animationInstance = lottie.loadAnimation({
        ...cachedSettings,
        container,
      });

      setAnimation(animationInstance);
    }

    return () => {
      animationInstance?.destroy();
      setAnimation(null);
    };
  }, [containerId, cachedSettings]);

  return { animation };
};

const lottieSettings: LottieSettings = {
  renderer: 'svg',
  loop: true,
  autoplay: true,
  animationData: snowMan,
};

export const Loading = () => {
  useAnimation({
    containerId: 'animation',
    lottieSettings,
  });

  return (
    <LoadingWrapper>
      <AnimationContainer id="animation" />
      <Text>Das Meisterwerk ist fast fertig...</Text>
    </LoadingWrapper>
  );
};
