import React, { ReactElement, useEffect } from 'react';
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';
import { ResultImageComponent } from '../ResultImageComponent';
import { NewSplitView } from '../../../NewSplitView';
import { SharingBar } from '../../../SharingBar';
import xmasGimmickSplitViewImage from '../../../../assets/images/xmas-gimmick/calendar.webp';
import xmasGimmickSplitViewImageMobile from '../../../../assets/images/xmas-gimmick/calendar-mobile.webp';
import { FaceSwapObject } from '../types';
import { Genders } from '../../../../helpers/nachhaltigkeit/weihnachtselfen/api';

export interface ResultScreenProps {
  faceSwapResult: FaceSwapObject;
  setFaceSwapResult: React.Dispatch<React.SetStateAction<FaceSwapObject | null>>
}

interface ResultText {
  title: string;
  text: string;
  splitViewText: string;
}

const ResultScreenContainer = styled.div`
  text-align: center;
`;

const ActionCtaText = styled.div`
  margin-top: -35px;
  margin-right: 20px;
  padding-bottom: 50px;
  color: #19232d;
  text-transform: uppercase;
  text-decoration: underline;
  letter-spacing: 3px;
  font-size: 10px;
  font-family: Poppins;
  cursor: pointer;

  @media screen and (max-width: 768px) {
    margin: -5px 0 0 5px;
    padding-bottom: 10px;
  }
`;

const CouponText = styled.p`
  font-size: 16px;
  font-family: Poppins;
  font-style: italic;
`;

const ResultActions = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const renderSharingBar = (): ReactElement => {
  const urls = 'https://www.uptodate.de/nachhaltigkeit/weihnachtselfen,https://www.uptodate.de/products/nachhaltiger-adventskalender';

  const message = `
  Hilf dem Weihnachtsmann mit zwei einfachen Schritten:
    1. Teile Dein Bild und hole noch mehr Elfen ins Boot. {url}
    2. Verschenke Nachhaltigkeit mit unserem Adventskalender und mache die Welt ein kleines bisschen besser. {url1}
  `;

  const emailSubject = 'Rette den Weihnachtsmann ';
  const emailBody = `
  Werde zum Weihnachtselfen und hilf dem Weihnachtsmann mit zwei einfachen Schritten:
    1. Teile Dein Bild und hole noch mehr Elfen ins Boot. {url}
    2. Verschenke Nachhaltigkeit mit unserem Adventskalender, der Dir dabei hilft nachhaltige Routinen aufzubauen und so die Welt ein kleines bisschen besser macht. {url1}
  `;

  return (
    <SharingBar
      urls={urls}
      message={message}
      emailSubject={emailSubject}
      emailBody={emailBody}
      ctaTextTop="Jetzt teilen"
      emailIconTrackingId="ga-weihnachtselfen-email-share-click"
      whatsAppIconTrackingId="ga-weihnachtselfen-whatsapp-share-click"
      facebookIconTrackingId="ga-weihnachtselfen-facebook-share-click"
      twitterIconTrackingId="ga-weihnachtselfen-twitter-share-click"
    />
  );
};

const selectTextFromResult = (faceSwapResult: FaceSwapObject): ResultText => {
  switch (faceSwapResult.imageKey) {
    case `grumpy_${Genders.MALE}`: {
      return {
        title: 'Grumpy Grinch',
        text: 'Auch wenn Du als Elf dem Grumpy Grinch ähnelst – vergiss nicht, dass der Grinch heimlich Weihnachten sehr geliebt hat!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    case `grumpy_${Genders.FEMALE}`: {
      return {
        title: 'Grumpy Grinch',
        text: 'Auch wenn Du als Elf dem Grumpy Grinch ähnelst – vergiss nicht, dass der Grinch heimlich Weihnachten sehr geliebt hat!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    case `happy_${Genders.MALE}`: {
      return {
        title: 'Fröhlicher Franz',
        text: 'Dein Lächeln ist so erwärmend, man könnte fast meinen Du solltest Dich als Elf lieber vom Nordpol fernhalten. Aber Spaß bei Seite, hier gilt es schließlich das Zuhause des Weihnachtsmanns zu retten!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    case `happy_${Genders.FEMALE}`: {
      return {
        title: 'Fröhliche Frieda',
        text: 'Dein Lächeln ist so erwärmend, man könnte fast meinen Du solltest Dich als Elf lieber vom Nordpol fernhalten. Aber Spaß bei Seite, hier gilt es schließlich das Zuhause des Weihnachtsmanns zu retten!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    case `surprised_${Genders.MALE}`: {
      return {
        title: 'Begeisterter Benny',
        text: 'So überrascht wie Du schauen sonst nur die Erwachsenen drein, die im Glauben, dass es den Weihnachtsmann nicht gibt, zu einem nächtlichen Snack in die Küche marschieren und ihn im Kamin feststecken sehen!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    case `surprised_${Genders.FEMALE}`: {
      return {
        title: 'Begeisterte Belinda',
        text: 'So überrascht wie Du schauen sonst nur die Erwachsenen drein, die im Glauben, dass es den Weihnachtsmann nicht gibt, zu einem nächtlichen Snack in die Küche marschieren und ihn im Kamin feststecken sehen!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    case `super_happy_${Genders.MALE}`: {
      return {
        title: 'Quickfideler Konstantin',
        text: 'Dein dauerzufriedenes Lächeln zeugt von einer tiefen Weihnachtszufriedenheit – oder doch von der geheimen Zutat in Deinen Plätzchen? Was es auch sein mag, nutze Deine Gabe, um das Zuhause des Weihnachtsmanns zu retten!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    case `super_happy_${Genders.FEMALE}`: {
      return {
        title: 'Quickfidele Katrin',
        text: 'Dein dauerzufriedenes Lächeln zeugt von einer tiefen Weihnachtszufriedenheit – oder doch von der geheimen Zutat in Deinen Plätzchen? Was es auch sein mag, nutze Deine Gabe, um das Zuhause des Weihnachtsmanns zu retten!',
        splitViewText: 'Mache die Welt etwas besser und verschenke Nachhaltigkeit mit unserem Adventskalender. Der hilft dabei, nachhaltige Routinen in den Alltag zu integrieren und somit langfristig zu dem Erreichen der Klimaziele beizutragen. Denn konsumieren ist schön, aber dabei etwas zu verändern ist besser.',
      };
    }
    default: {
      return {
        title: '',
        text: '',
        splitViewText: '',
      };
    }
  }
};

export const XmasResultScreen: React.FC<ResultScreenProps> = ({ faceSwapResult, setFaceSwapResult }) => {
  const resultText = selectTextFromResult(faceSwapResult);

  const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

  useEffect(() => {
    window.scrollTo(0, 250);
  }, []);

  return (
    <ResultScreenContainer>
      <ResultImageComponent
        title={resultText.title}
        subTitle={resultText.text}
        image={faceSwapResult.image}
      />
      {renderSharingBar()}
      <ResultActions>
        <ActionCtaText><a download="xmas-elf.jpeg" href={`data:image/jpeg;base64,${faceSwapResult.image}`} data-tracking-id="ga-weihnachtselfen-calendar-image-download">jetzt downloaden</a></ActionCtaText>
        <ActionCtaText
          onClick={() => setFaceSwapResult(null)}
        >
          Neues Bild
        </ActionCtaText>
      </ResultActions>
      <NewSplitView
        title="Was kannst du als Elf tun, um den Weihnachtsmann zu helfen?"
        imageUrl={isMobile ? xmasGimmickSplitViewImageMobile : xmasGimmickSplitViewImage}
        imagePosition="left"
        ctaText="Adventskalender kaufen"
        ctaUrl="https://www.uptodate.de/products/nachhaltiger-adventskalender"
        ctaTrackingId="ga-weihnachtselfen-goto-calendar"
        isMobile={isMobile}
      >
        <div>
          <p>{resultText.splitViewText}</p>
          <CouponText>
            Jetzt <b>5€</b> mit dem Code <b>Adventskalender5</b> sparen.
          </CouponText>
        </div>
      </NewSplitView>
    </ResultScreenContainer>
  );
};
