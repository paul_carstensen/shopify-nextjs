import React, { useEffect, useState } from 'react';
import { Button, SlideProps, Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import styled from 'styled-components';
import { SliderContainer } from '../../../SliderContainer';
import { SlideIntro } from '../../../SlideIntro';

const StyledSmartricityIframe = styled.iframe`
  overflow: hidden; 
  width: 100%; 
  border: none;
`;

const StyledButton = styled.div`
  margin-right: 20px;
  display: inline-block;

  @media screen and (max-width: 768px) {
    margin-bottom: 20px;
  }
`;

export const SliderSmartricity: React.FC<SlideProps & { nextTipp: () => void; }> = ({ nextTipp }) => {
  const [consentGiven, setConsentGiven] = useState(false);

  useEffect(() => {
    window.addEventListener('message', (e) => {
      if (typeof e !== 'undefined') {
        const message = e.data;
        if (message.type === 'smtc-energy-efficiency-iframe-resize') {
          const iframe = window.document.querySelector('iframe');
          if (iframe) {
            iframe.height = message.height + 50;
          }
        }
      }
    });
  }, [consentGiven]);

  return (
    <SliderContainer
      counter="3"
      numSlides="5"
      id="tip-3"
      backgroundColor="#ffffff"
      title="Weißt Du eigentlich wie viel Deine Geräte verbrauchen?"
      watermarkBg
    >
      <Slider visibleSlidesBreakpoints={1} enableArrows={false} enableMouseWheel={false}>
        <SlideIntro
          padding={0}
          text="Einige wirklich alte Haushaltsgeräte entpuppen sich häufig als unangenehme Stromfresser, die große Mengen an Energie verbrauchen und damit nicht nur Deinen Geldbeutel, sondern auch die Umwelt belasten. Mit unserem Partner Smartricity kannst Du überprüfen, wie hoch Dein Einsparpotential wäre, wenn Du Dein Altgerät mit einem energieeffizienten Neugerät ersetzt."
          startButtonTrackingId="ga-sust-for-beginners-smartricity-continue"
          onSkipClick={nextTipp}
          skipButtonText="Nein, lieber ein anderer Tipp"
          skipButtonTrackingId="ga-sust-for-beginners-smartricity-skip"
        />
        <div id="nachhaltige_analysen_iframe">
          {!consentGiven
            && (
              <>
                <p> Wir benötigen Deine Einwilligungen, um diesen von der Smartricity GmbH, Bahnhofstr. 10, 94032
                  Passau, bereitgestellten Inhalt anzuzeigen: Wenn Du das Tool von Smartricity nutzt, werden Cookies eingesetzt
                  und personenbezogene Daten (insb. Deine IP-Adresse) auch von Smartricity zur Bereitstellung des Tools
                  verarbeitet. Für weitere Informationen siehe unsere <a href="/pages/privacy-policy" target="_blank">DATENSCHUTZINFORMATIONEN NACH ART. 13, 14 DSGVO</a> sowie die Datenschutzerklärungen von Smartricity,
                  <a
                    href="https://smartricity.de/datenschutz"
                    target="_blank"
                    rel="noreferrer"
                  >https://smartricity.de/datenschutz
                  </a> und
                  <a href="https://www.zuhause-bleiben.de/datenschutz" target="_blank" rel="noreferrer"> Zuhause Bleiben gegen Corona</a>, insb.
                  zu Deinem Recht, die Einwilligungen jederzeit formlos widerrufen zu können, sowie zu Deinen Einstellungs- und
                  Widerspruchsmöglichkeiten und Deinen weiteren Rechten.
                </p>
                <StyledButton>
                  <Button data-testid="btn-smartricity-consent" trackingId="ga-sust-for-beginners-smartricity-consent" appearance="secondary" onClick={() => setConsentGiven(true)}>Gerätecheck starten und Einwilligung</Button>
                </StyledButton>
              </>
            )}
          {consentGiven
            && (
              <div id="smtc-energy-efficiency-iframe-container">
                <StyledSmartricityIframe
                  src="https://energie-effizienz-iframe.smartricity.de/?customerID=uptodate_de"
                  id="smtc-energy-efficiency-iframe"
                />
                <div id="smtc-energy-efficiency-iframe-attribution">
                  Powered by
                  <a
                    href="https://www.smartricity.de/"
                    target="_blank"
                    rel="noreferrer"
                  >Smartricity
                  </a>
                </div>
                <Button trackingId="ga-sust-for-beginners-smartricity-finish" onClick={nextTipp}>Fertig</Button>
              </div>
            )}
        </div>
      </Slider>
    </SliderContainer>
  );
};
