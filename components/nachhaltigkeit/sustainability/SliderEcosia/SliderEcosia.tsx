import React, { useEffect, useState } from 'react';
import { Slider, Button } from '@u2dv/marketplace_ui_kit/dist/components';
import { browserName, isMobile } from 'react-device-detect';
import styled from 'styled-components';
import { Select } from './Select';
import { SlideIntro } from '../../../SlideIntro';
import { SliderContainer } from '../../../SliderContainer';

const StyledFirstSlideContainer = styled.div`
  padding: 0 32px;
`;

const StyledUL = styled.ul`
  margin: 20px 0px;
  list-style-type: decimal;
  padding-left: 15px;
`;

const StyledButton = styled(Button)`
  margin-right: 20px;

  @media screen and (max-width: 768px) {
    margin-top: 20px;
  }
`;

const StyledTippContainer = styled.div`
  margin-left: -32px;
  margin-right: -32px;

  & h3 {
    text-transform: none;
    font-weight: 600;
  }
`;

export interface SustainabilitySliderEcosiaProps {
  nextTipp: any,
}

const options = [
  { title: 'Google Chrome', value: 'Chrome' },
  { title: 'Google Chrome (mobile)', value: 'Chrome-mobile' },
  { title: 'Safari', value: 'Safari' },
  { title: 'Safari (mobile)', value: 'Safari-mobile' },
];

type SelectionSlideProps = {
  text: string;
  value?: string;
  subtext: string
  onChange: (value: string) => void;
  nextSlide?: () => void;
}

const SelectionSlide = ({ nextSlide, value, onChange, text, subtext }: SelectionSlideProps) => (
  <StyledFirstSlideContainer data-testid="select-browser">
    <p>
      {text}
    </p>
    <p>
      {subtext}
    </p>
    {value && (
      <Select
        value={value}
        options={options}
        onSelectChange={(e) => onChange(e.target.value)}
      />
    )}
    <StyledButton data-testid="btn-ecosia-continue2" trackingId="" onClick={nextSlide}>Auf Ecosia umstellen</StyledButton>
  </StyledFirstSlideContainer>
);

const ItemList: React.FC<{ items: string[] }> = ({ items }) => (
  <StyledUL>
    {items.map((content) => <li key={content}>{content}</li>)}
  </StyledUL>
);

export const SliderEcosia: React.FC<SustainabilitySliderEcosiaProps> = ({ nextTipp }) => {
  const [browser, setBrowser] = useState<string | undefined>(undefined);
  const chromeItems = ['Öffne das Menü, indem Du auf die 3 Punkte in der oberen rechte Ecke klickst - dann wähle “Einstellungen”.', 'Wähle "Suchmaschinen verwalten".', 'Finde Ecosia unter "Suchmaschinen".'];
  const chromeMobileItems = ['Öffne das Menü, indem Du auf die 3 Punkte in der oberen rechte Ecke klickst - dann wähle “Einstellungen”.', 'Klicke auf "Suchmaschine".', 'Wähle dann Ecosia aus der Liste aus'];
  const safariItems = ['Öffne die Safari Einstellungen über die Menuleiste.', 'Klicke auf das Such-Symbol.', 'Wähle Ecosia unter "Suchmaschinen" aus.'];
  const safariMobileItems = ['Gehe zu iOS-Einstellungen', 'Scrolle nach unten zu Safari > Suchmaschine', 'Wähle Ecosia'];

  const introText = 'Mach Ecosia zu deiner Suchmaschine. Ecosia ist eine kostenlose Alternative zu Google, die für Deine Suchanfragen Bäume pflanzt. Für ca. 45 Suchanfragen gibt es einen Baum.';

  useEffect(() => {
    if (!browser) {
      if (browserName === 'Chrome') {
        setBrowser(`Chrome${isMobile ? '-mobile' : ''}`);
      } else if (browserName === 'Safari') {
        setBrowser(`Safari${isMobile ? '-mobile' : ''}`);
      } else {
        setBrowser('Chrome');
      }
    }
  }, [setBrowser, browser]);

  return (
    <SliderContainer
      title="Bäume pflanzen beim Suchen"
      backgroundColor="#98b6a0"
      counter="1"
      numSlides="5"
      watermarkBg
    >
      <StyledTippContainer>
        <Slider visibleSlidesBreakpoints={1} enableArrows={false} enableMouseWheel={false}>
          <SlideIntro
            padding={32}
            text={introText}
            startButtonTrackingId="ga-sust-for-beginners-ecosia-continue"
            onSkipClick={nextTipp}
            skipButtonText="Nein, lieber ein anderer Tipp"
            skipButtonTrackingId="ga-sust-for-beginners-ecosia-skip"
          />
          <Slider visibleSlidesBreakpoints={1} enableArrows={false} enableMouseWheel={false}>
            <SelectionSlide
              text={introText}
              subtext="Wähle Deinen Browser:"
              value={browser}
              onChange={(browserSelection: string) => setBrowser(browserSelection)}
            />
            <StyledFirstSlideContainer>
              <p>
                Wähle Deinen Browser:
              </p>
              {browser && (
                <Select
                  value={browser}
                  options={options}
                  onSelectChange={(e) => setBrowser(e.target.value)}
                />
              )}
              {browser === 'Chrome' && <ItemList items={chromeItems} />}
              {browser === 'Chrome-mobile' && <ItemList items={chromeMobileItems} />}
              {browser === 'Safari' && <ItemList items={safariItems} />}
              {browser === 'Safari-mobile' && <ItemList items={safariMobileItems} />}
              <StyledButton data-testid="btn-ecosia-finish" trackingId="ga-sust-for-beginners-ecosia-finish" onClick={nextTipp}>Fertig</StyledButton>
            </StyledFirstSlideContainer>
          </Slider>
        </Slider>
      </StyledTippContainer>
    </SliderContainer>
  );
};
