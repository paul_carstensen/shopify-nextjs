import React from 'react';
import styled from 'styled-components';
import { Button, SlideProps, Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import { SliderContainer } from '../../../SliderContainer';
import { SlideIntro } from '../../../SlideIntro';

const StyledTippCotainer = styled.div`
  margin-left: -32px;
  margin-right: -32px;

  & h3 {
    margin: 0 0 7.5px;
    font-family: "Quicksand", sans-serif;
    letter-spacing: 0em;
    line-height: 1;
    font-size: 1.4em;
    @media only screen and (min-width: 769px) {
      margin: 0 0 15px;
    }
    text-transform: none;
    font-weight: 600;
  }
`;

export const SliderAvoidWaste: React.FC<SlideProps & { nextTipp: () => void; }> = ({ nextTipp }) => (
  <SliderContainer
    counter="2"
    numSlides="5"
    backgroundColor="#ede3bb"
    title="Reduce, Reuse, Recycle"
    watermarkBg
  >
    <StyledTippCotainer data-testid="slider-tipps">
      <Slider visibleSlidesBreakpoints={1} enableArrows={false} enableMouseWheel={false}>
        <SlideIntro
          padding={32}
          text="Ein nachhaltiger Alltag setzt sich aus vielen nachhaltigen Taten zusammen. Deinen Fokus in jedem Lebensbereich ein wenig zu ändern, kann Deinen CO2-Fußabdruck insgesamt stark verringern. Und wenn jeder mithilft, machen wir zusammen einen echten Unterschied. Dein Zuhause ist der Ort, an dem Du anfangen kannst, nachhaltiger zu leben. Hier die 5 wirkungsvollsten Tipps!"
          startButtonTrackingId="ga-sust-for-beginners-avoid-waste-continue"
          onSkipClick={nextTipp}
          skipButtonText="Nein, lieber ein anderer Tipp"
          skipButtonTrackingId="ga-sust-for-beginners-avoid-waste-skip"
        />

        <Slider visibleSlidesBreakpoints={1} enableArrows enableMouseWheel>
          <div>
            <h3>Tipp 1: Verwende weniger Plastik und Einwegprodukte</h3>
            <p>Plastikmüll verursacht große Umweltprobleme. Dabei kann man mit ein paar einfachen Maßnahmen viel Müll vermeiden. Verwende Mehrweg-Taschen zum Einkaufen, trinke Wasser aus der Leitung und kaufe Obst und Gemüse unverpackt auf dem lokalen Wochenmarkt.</p>
          </div>
          <div>
            <h3>Tipp 2: Fleischkonsum reduzieren</h3>
            <p>Pro Kopf werden in Deutschland über 320 kg CO2-Emissionen pro Jahr durch Fleischkonsum verursacht. Das liegt vor allem an der Viehzucht und der Tierhaltung. Natürlich musst Du Dich nicht von heute auf morgen vegetarisch oder vegan ernähren, aber versuche, ein paar “fleischfreie Tage” in die Woche einzubauen. So tust Du der Umwelt etwas gutes - und Deinem Körper auch!</p>
          </div>
          <div>
            <h3>Tipp 3: Saisonal & regional Essen</h3>
            <p>Kaufe Deine Lebensmittel regional und saisonal. Du ernährst Dich dadurch gesund und frisch und unterstützt obendrein auch Deine Region. Importierte Lebensmittel verursachen sehr viel CO2 durch die weiten Transportwege. Dein regionaler Einkauf verhindert das. </p>
          </div>
          <div>
            <h3>Tipp 4: Reparieren statt wegwerfen</h3>
            <p>Kaputte Reißverschlüsse, verlorene Knöpfe, oder Löcher können repariert werden, ohne dass Du Dein Lieblingsstück gleich wegwerfen musst. - Das macht oft sogar viel Spaß!<br />Egal ob Kleidung, Sportgeräte oder technische Geräte - versuche die Dinge zu reparieren, bevor Du sie wegwirfst!</p>
          </div>
          <div>
            <h3>Tipp 5: Weniger Lebensmittel wegwerfen</h3>
            <p>Plane Deine Mahlzeiten im Voraus, dann bleibt hinterher weniger übrig. Lagere Deine Lebensmittel richtig, dann halten sie länger. Unterstütze Foodsharing-Initiativen in Deiner Nähe.</p>
            <Button data-testid="btn-avoid-waste-finish" trackingId="ga-sust-for-beginners-avoid-waste-finish" onClick={nextTipp}>Fertig</Button>
          </div>
        </Slider>
      </Slider>
    </StyledTippCotainer>
  </SliderContainer>
);
