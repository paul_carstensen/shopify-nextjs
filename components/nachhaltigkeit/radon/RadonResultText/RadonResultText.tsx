import React from 'react';

export type RadonResultTextProps = {
  radonLevel: number;
}

const getRadonResultText = (radonLevel: number) => {
  if (radonLevel < 40) {
    return (
      <p>
        Das Radonpotenzial in Deiner Gegend ist als gering einzustufen (kleiner 40).
        Messungen in vergleichbaren Gesteinseinheiten haben gezeigt, dass mit niedrigem
        bzw. mäßigem Radonpotenzial zu rechnen ist.

        Bereits bei mäßigem Radonpotenzial, vor allem bei guter Gaspermeabilität des
        Bodens, wird aber ein der Radonsituation angepasstes Bauen empfohlen. Es kann
        auch nicht ausgeschlossen werden, dass unter dem Baugebiet eine geologische
        Störung vorliegt. Orientierende Radonmessungen in der Bodenluft in Abhängigkeit
        von den geologischen Gegebenheiten des Bauplatzes oder Baugebietes sollten die
        Information liefern, ob das Thema Radon bei der Bauplanung entsprechend zu
        berücksichtigen ist.
      </p>
    );
  }

  if (radonLevel >= 40 && radonLevel <= 100) {
    return (
      <p>
        Das Plangebiet, in dem Dein Haus liegt, ist innerhalb eines Bereiches, in
        dem lokal auch erhöhtes und seltener hohes Radonpotenzial über einzelnen
        Gesteinshorizonten ermittelt wurde (Radonpotenzial zw. 40 und 100).

        Es wird dringend empfohlen orientierende Radonmessungen in der Bodenluft
        vorzunehmen, um festzustellen, ob und in welchem Ausmaß Baumassnahmen der
        jeweiligen lokalen Situation angepasst werden sollten.
      </p>
    );
  }

  if (radonLevel > 100) {
    return (
      <p>
        Das Gebiet, in dem Dein Haus liegt, ist innerhalb eines Bereiches in dem
        ein erhöhtes bis hohes Radonpotenzial bekannt ist bzw. nicht ausgeschlossen
        werden kann (Radonpotenzial größer 100). Radonmessungen in der Bodenluft des
        Bauplatzes oder Baugebietes werden dringend empfohlen. Die Ergebnisse sollten
        Grundlage für die Bauplaner und Bauherren sein, sich ggf. für bauliche
        Vorsorgemaßnahmen zu entscheiden. (Anmerkung: der Begriff “lokal“ bedeutet
        hierbei, dass ein erhöhtes bis hohes Radonpotenzial meist eng an
        geologisch-tektonische Einheiten gebunden ist. Solche Bereiche besitzen deshalb
        eine sehr begrenzte Ausdehnung. Die Berechnung des Radonpotenzials beruht bisher
        auf nur wenigen Messungen und ist deshalb nur zur groben Orientierung geeignet.
        Lokal sind starke Abweichungen von dem dargestellten Radonpotenzial möglich.
        Das angegebene Radonpotenzial kann daher nicht Grundlage der Bauplanung sein,
        sondern es bedarf der gesonderten Untersuchungen.
      </p>
    );
  }

  return null;
};

export const RadonResultText: React.FC<RadonResultTextProps> = ({ radonLevel }) => {
  const resultText = getRadonResultText(radonLevel);

  if (!resultText) {
    return <p>Wir konnten Dein Ergebnis nicht ermitteln. Bitte versuche es noch einmal.</p>;
  }

  return (
    <>
      {resultText}
      <span>Quelle: Bundesamt für Strahlenschutz.</span>
    </>
  );
};
