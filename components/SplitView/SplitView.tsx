import React, { MouseEvent, MouseEventHandler } from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import styles from './SplitView.module.css';
import { PageWidthContainer } from '../common/PageWidthContainer';
import { ButtonLinkDark } from '../common/Buttons';
import { HeaderAnchorOffset } from '../common/HeaderAnchorOffset';
import { cleanElementId } from '../../helpers/cleanElementId';

const SplitViewButton = styled(ButtonLinkDark)`
  display: block;
  margin-top: 20px;

  @media screen and (max-width: 768px) {
    margin: auto;
  }
`;

export interface SplitViewProps {
  children: React.ReactNode;
  title: string;
  imageUrl: any;
  ctaTitle?: string;
  ctaText?: string;
  ctaUrl?: string;
  ctaNewWindow?: boolean;
  dataTrackingId?: string;
  onCtaButtonClick?: () => any;
  imagePosition?: 'left' | 'right';
  id?: string;
}

export const SplitView: React.FC<SplitViewProps> = (props: SplitViewProps) => {
  const {
    children, title, ctaTitle, imageUrl, ctaText, ctaUrl, ctaNewWindow, onCtaButtonClick, imagePosition, dataTrackingId, id,
  } = props;

  const handleCtaButtonClick: MouseEventHandler<HTMLAnchorElement> = (event: MouseEvent<HTMLAnchorElement>) => {
    event.preventDefault();

    onCtaButtonClick?.();
  };

  const renderBtn = () => (
    // eslint-disable-next-line react/jsx-no-target-blank
    <SplitViewButton
      role="button"
      href={ctaUrl}
      target={ctaNewWindow ? '_blank' : undefined}
      rel={ctaNewWindow ? 'noopener noreferrer' : undefined}
      onClick={onCtaButtonClick ? handleCtaButtonClick : undefined}
      data-tracking-id={dataTrackingId}
    >
      {ctaText}
    </SplitViewButton>
  );
  const shouldRenderBtn = ctaText && (ctaUrl || onCtaButtonClick);

  return (
    <HeaderAnchorOffset id={id ?? cleanElementId(`split_view_${title}`)}>
      <PageWidthContainer>
        <div className={styles['split-view-inner-container']}>
          <div className={`${styles.splitView} ${styles[`image-position-${imagePosition || 'right'}`]}`}>
            <div className={styles.content}>
              <div className={styles.title}>{title}</div>
              {children}
              {ctaTitle && <div className={styles.title}>{ctaTitle}</div>}
              {shouldRenderBtn && renderBtn()}
            </div>
            <div className={styles.image}>
              <Image src={imageUrl} alt="" width="640" height="645" />
            </div>
          </div>
        </div>
      </PageWidthContainer>
    </HeaderAnchorOffset>
  );
};
