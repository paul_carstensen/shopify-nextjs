import React from 'react';
import styled from 'styled-components';
import styles from './ProductHighlights.module.css';
import ProductHighlight from './ProductHighlight';
import { PageWidthContainer } from '../common/PageWidthContainer';
import { ButtonLinkLight } from '../common/Buttons';

type ProductHighlightsProps = {
  products: any[];
  collectionHandle: string;
  title?: string;
}

const ProductHighlightsTitle = styled.h2`
  margin: 0 0 7.5px;
  font-family: "Quicksand", sans-serif;
  font-weight: 400;
  letter-spacing: 0em;
  line-height: 1;
  text-transform: uppercase;
  font-size: 1.72em;
  @media only screen and (min-width: 769px) {
    margin: 0 0 15px;
  }
`;

const ProductHighlightsButton = styled(ButtonLinkLight)`
  display: block;
  margin: 2rem auto 0 auto;
  padding: 8px 14px;
  background-position: 150% 45%;
  min-width: 90px;
  font-size: 12px;
`;

const ProductHighlights: React.FC<ProductHighlightsProps> = ({ collectionHandle, products, title = 'Unsere Produkt-Highlights' }) => (
  <div className={styles.suggestion_container}>
    <PageWidthContainer>
      <div className={styles.header}>
        {title && (
        <ProductHighlightsTitle className={styles.title}>
          {title}
        </ProductHighlightsTitle>
        )}
        <ProductHighlightsButton
          href={`/collections/${collectionHandle}`}
          className="alle-anzeigen-btn"
          data-tracking-id="ga-blog-product-highlights-view-all"
        >
          Alle Anzeigen
        </ProductHighlightsButton>
      </div>
      <div className={styles.grid}>
        {products.map(({ imageUrl, title: productTitle, price, originalPrice, handle }) => (
          <ProductHighlight
            key={handle}
            imageUrl={imageUrl}
            title={productTitle}
            price={price}
            originalPrice={originalPrice}
            handle={handle}
            collectionHandle={collectionHandle}
          />
        ))}
      </div>
    </PageWidthContainer>
  </div>
);

export default ProductHighlights;
