import React from 'react';
import { NavigationItem } from '../../../../types';

const EXCLUDED_SIDE_LINKS = ['Earnest App', 'Über uns'];

type SecondaryMenuItemProps = {
  delay: number;
  link: NavigationItem;
}

const SecondaryMenuItem: React.FC<SecondaryMenuItemProps> = ({ delay, link }) => (
  <div className={`grid__item one-half appear-animation appear-delay-${delay} medium-up--hide`}>
    <a href={link.url} className="mobile-nav__link">{link.title}</a>
  </div>
);

type SecondaryMenuProps = {
  delayOffset: number;
  sideNavigationItems: NavigationItem[];
}

export const SecondaryMenu: React.FC<SecondaryMenuProps> = ({ delayOffset, sideNavigationItems }) => {
  const sideNavigations = sideNavigationItems.filter((link) => !EXCLUDED_SIDE_LINKS.includes(link.title));

  return (
    <li className="mobile-nav__item mobile-nav__item--secondary">
      <div className="grid">
        {sideNavigations.map((link, i) => (<SecondaryMenuItem link={link} delay={delayOffset + i + 1} key={`mobile-secondary-headerlinks-${link.title}`} />))}
      </div>
    </li>
  );
};
