import React, { useState } from 'react';
import styled from 'styled-components';

const Select = styled.select<{ width?: string }>`
  align-self: center;
  margin-bottom: 15px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background-position: right center;
  background-image: url(//cdn.shopify.com/s/files/1/0268/4819/8771/t/80/assets/ico-select.svg);
  background-repeat: no-repeat;
  background-position: right 10px center;
  background-size: 11px;
  text-indent: 0.01px;
  text-overflow: '';
  cursor: pointer;
  color: inherit;
  ${(props) => `width: ${props.width};`}
  @media screen and (max-width: 768px) {
    margin-bottom: 10px;
    width: calc(100% - 38px);
  }
`;

const Question = styled.div<{ maxWidth?: string; }>`
  align-self: center;
  margin-bottom: 15px;
  ${(props) => `max-width: ${props.maxWidth};`}
  @media screen and (max-width: 768px) {
    margin-bottom: 10px;
    text-align: left;
    font-size: 12px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 3px;
  }
`;

const SelectOption = styled.option``;

export type Option = {
  disabled?: boolean;
  title: string;
  value: number | string;
};

export type SelectQuestionProps = {
  disabled?: boolean;
  value: number | string;
  options: Option[];
  question: string;
  questionMaxWidth?: string;
  questionWidth?: string;
  selectMaxWidth?: string;
  selectWidth?: string;
  onSelectChange?: (event?: any) => void;
};

const renderOption = ({ disabled, title, value }: Option) => (
  <SelectOption key={title} value={value} {...(disabled && { disabled: true, hidden: true })}>
    {title}
  </SelectOption>
);

export const SelectQuestion: React.FC<SelectQuestionProps> = ({
  disabled,
  value,
  options,
  question,
  questionMaxWidth,
  selectWidth,
  onSelectChange,
}: SelectQuestionProps) => {
  const [stateValue, setStateValue] = useState<number | string>(value);
  const select = (
    <Select
      onChange={(event) => {
        setStateValue(event.target.value);
        onSelectChange?.(event);
      }}
      width={selectWidth}
      disabled={!!disabled}
      value={stateValue}
    >
      {options.map(renderOption)}
    </Select>
  );
  return (
    <>
      <Question maxWidth={questionMaxWidth}>
        {question}
      </Question>
      {select}
    </>
  );
};
