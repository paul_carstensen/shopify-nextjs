import React, { useState } from 'react';
import styled from 'styled-components';
import { Explanation, Explanations } from 'components/Explanations';

const IntroContainer = styled.div`
  font-size: 14px;
  text-align: center;
`;

const Title = styled.h1`
  margin: 0 0 7.5px;
  font-family: "Quicksand", sans-serif;
  letter-spacing: 0em;
  line-height: 1;
  font-size: 14px;
  text-transform: inherit ;
  font-weight: bold;
`;

const Description = styled.p`
  font-size: 14px;
`;

const Button = styled.button`
  font-size: 14px;
  font-weight: bold;
  line-height: 22.4px;
`;

export type IntroProps = {
  title: string;
  description: React.ReactNode;
  dataTrackingId: string;
  explanations?: Explanation[];
}

export const Intro: React.FC<IntroProps> = ({ title, description, dataTrackingId, explanations }: IntroProps) => {
  const [showExplanations, setShowExplanations] = useState<boolean>(false);

  return (
    <>
      <IntroContainer>
        <Title>
          {title}
        </Title>
        <Description>
          {description}
          {
            explanations ? (
              <Button
                type="button"
                onClick={() => setShowExplanations(!showExplanations)}
                data-tracking-id={dataTrackingId}
              >
                {showExplanations ? '< Weniger' : '> Mehr'}
              </Button>
            ) : null
          }
        </Description>
      </IntroContainer>
      {
        explanations && showExplanations ? (
          <Explanations
            onClose={() => setShowExplanations(!showExplanations)}
            explanations={explanations}
          />
        ) : null
      }
    </>
  );
};
