import { BlogBlockRenderer, Element as ElementType } from './BlogBlockRenderer';

// Work around issue https://github.com/microsoft/TypeScript/issues/28481
export { BlogBlockRenderer };
export type Element = ElementType;
