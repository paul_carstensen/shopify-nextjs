import React from 'react';
import styled from 'styled-components';

const QuotedTextInner = styled.div<{textAlign?: string}>`
  background-color:#c4d6e6;
  display: flex;
  padding: 105px 65px;
  margin: 0px -20px;
  @media screen and (min-width: 769px) {
    margin: auto;
    font-size: 16px;
    line-height: 1.56;
    padding: 52px 100px;
    font-size: 22px;
    font-weight: 500;
    line-height: 1.14;
    ${(props) => `text-align: ${props.textAlign || 'center'};`}
  }
`;

const Quote = styled.div<{textAlign?: string}>`
  font-size: 56px;
  line-height: 0.14;
  font-weight: 500;
  margin-bottom: -20px;
  ${(props) => `align-self: ${props.textAlign || 'flex-start'};`}
`;

export interface QuotedTextProps {
  textAlign?: string;
  children: React.ReactNode;
}

export const QuotedText: React.FC<QuotedTextProps> = ({ textAlign, children }: QuotedTextProps) => (
  <QuotedTextInner textAlign={textAlign}>
    <Quote>“</Quote>{children}<Quote textAlign="flex-end">”</Quote>
  </QuotedTextInner>
);
