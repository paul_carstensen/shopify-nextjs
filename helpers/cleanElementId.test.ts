import { cleanElementId } from './cleanElementId';

describe('cleanElementId', () => {
  it('Lower case the whole id', () => {
    const result = cleanElementId('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    expect(result).toEqual('abcdefghijklmnopqrstuvwxyz');
  });

  it('Only keep dashes and underscores and replace other non alphabetic characters to _', () => {
    const result = cleanElementId('&é"\'(-è_çà)=üëäö~#{[|`\\^@]}?😃');
    expect(result).toEqual('_____-________________________');
  });
});
