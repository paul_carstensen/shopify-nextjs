import { fetchGraphQL, GraphQLRequestOptions } from '@u2dv/marketplace_common/dist/fetchGraphQL';
import { getCognitoAdminAuthToken, CognitoAdminAuthOptions } from './getCognitoAdminAuthToken';

export const fetchGraphQLAuthenticated = async (
  url: string,
  query: string,
  { headers, variables }: GraphQLRequestOptions,
  credentials: CognitoAdminAuthOptions,
) => {
  try {
    return await fetchGraphQL(url, query, {
      variables,
      headers: {
        ...headers,
        Authorization: await getCognitoAdminAuthToken(credentials, { useCached: true }),
      },
    });
  } catch (err) {
    if (err.message.startsWith('400') || err.message.startsWith('401') || err.message.startsWith('403')) {
      console.info(`GraphQL request failed (${err.message})! Trying to re-fetch token…`); // eslint-disable-line no-console
      return fetchGraphQL(url, query, {
        variables,
        headers: {
          ...headers,
          Authorization: await getCognitoAdminAuthToken(credentials, { useCached: false }),
        },
      });
    }

    throw err;
  }
};
