import fetch from 'cross-fetch';
import config from '../../../config';

export const getPolicyText = async (policyType: string, appId: number = 1): Promise<string> => {
  const policyUrl = `${config().consentTracker.apiUrl}/policy/text/${appId}/${policyType}/latest.html`;
  const response = await fetch(policyUrl);

  return response.text();
};
