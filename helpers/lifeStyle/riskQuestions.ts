import imageRiskQuestion1 from '../../assets/images/risk/riskQuestion_1.jpg';
import imageRiskQuestion2 from '../../assets/images/risk/riskQuestion_2.jpg';
import imageRiskQuestion3 from '../../assets/images/risk/riskQuestion_3.jpg';
import imageRiskQuestion4 from '../../assets/images/risk/riskQuestion_4.jpg';
import imageRiskQuestion5 from '../../assets/images/risk/riskQuestion_5.jpg';
import imageRiskQuestion6 from '../../assets/images/risk/riskQuestion_6.jpg';
import imageRiskQuestion7 from '../../assets/images/risk/riskQuestion_7.jpg';
import imageRiskQuestion8 from '../../assets/images/risk/riskQuestion_8.jpg';
import imageRiskQuestion9 from '../../assets/images/risk/riskQuestion_9.jpg';
import imageRiskQuestion10 from '../../assets/images/risk/riskQuestion_10.jpg';

const RiskQuestions = [
  {
    questions: 'Ich bin ein Pionier des Wandels',
    bgImage: imageRiskQuestion1.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'online_avatar', 'visionary', 'storyteller',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'le_hipster', 'hip_and_hopper', 'world_pilot',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'solo_dj', 'multitasker', 'urban_guy',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'freedom_mind', 'luxury_lover', 'yogi_guru',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'mind_maker', 'social_politician', 'silver_athlete',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'gatekeeper', 'sun_herdsman', 'autopilot',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Ich stehe für Achtsamkeit und Werte der Entschleunigung',
    bgImage: imageRiskQuestion2.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'yogi_guru', 'le_hipster', 'mind_maker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'sun_herdsman', 'social_politican', 'silver_athlete',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'autopilot', 'gatekeeper', 'storyteller',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'visionary', 'urban_guy', 'world_pilot',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'freedom_mind', 'online_avatar', 'luxury_lover',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'solo_dj', 'hip_and_hopper', 'multitasker',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Ich bin ein Trendsetter',
    bgImage: imageRiskQuestion3.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'urban_guy', 'online_avatar', 'storyteller',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'world_pilot', 'visionary', 'le_hipster',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'hip_and_hopper', 'luxury_lover', 'multitasker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'solo_dj', 'social_politican', 'silver_athlete',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'yogi_guru', 'sun_herdsman', 'gatekeeper',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'autopilot', 'freedom_mind', 'mind_maker',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Ich stehe für Nachhaltigen Wachstum',
    bgImage: imageRiskQuestion4.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'visionary', 'mind_maker', 'le_hipster',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'freedom_mind', 'yogi_guru', 'social_politican',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'autopilot', 'sun_herdsman', 'gatekeeper',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'urban_guy', 'storyteller', 'silver_athlete',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'online_avatar', 'luxury_lover', 'world_pilot',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'solo_dj', 'hip_and_hopper', 'multi-performer',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Ich interessiere mich für zukünftiger Wohnkonzepte ',
    bgImage: imageRiskQuestion5.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'world_pilot', 'urban_guy', 'sun_herdsman',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'yogi_guru', 'le_hipster', 'hip_and_hopper',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'freedom_mind', 'luxury_lover', 'visionary',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'online_avatar', 'multitasker', 'mind_maker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'solo_dj', 'gatekeeper', 'autopilot',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'silver_athlete', 'social_politican', 'storyteller',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Ich begebe mich auf Kulinalische Abenteuer',
    bgImage: imageRiskQuestion6.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'urban_guy', 'multitasker', 'social_politican',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'world_pilot', 'le_hipster', 'silver_athlete',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'luxury_lover', 'sun_herdsman', 'mind_maker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'hip_and_hopper', 'yogi_guru', 'autopilot',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'freedom_mind', 'visionary', 'gatekeeper',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'online_avatar', 'storyteller', 'solo_dj',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Ich bin Digital affin',
    bgImage: imageRiskQuestion7.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'online_avatar', 'world_pilot', 'multitasker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'luxury_lover', 'hip_and_hopper', 'visionary',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'freedom_mind', 'yogi_guru', 'autopilot',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'urban_guy', 'mind_maker', 'le_hipster',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'storyteller', 'silver_athlete', 'solo_dj',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'gatekeeper', 'sun_herdsman', 'social_politican',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Mein Gesundheitsbewusstsein prägt meinen Alltag',
    bgImage: imageRiskQuestion8.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'yogi_guru', 'silver_athlete', 'multitasker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'freedom_mind', 'sun_herdsman', 'mind_maker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'autopilot', 'social_politican', 'le_hipster',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'online_avatar', 'hip_and_hopper', 'world_pilot',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'luxury_lover', 'urban_guy', 'gatekeeper',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'solo_dj', 'visionary', 'storyteller',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Familie geht über alles',
    bgImage: imageRiskQuestion9.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'freedom_mind', 'storyteller', 'sun_herdsman',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'multitasker', 'le_hipster', 'gatekeeper',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'yogi_guru', 'autopilot', 'silver_athlete',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'hip_and_hopper', 'visionary', 'social_politican',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'online_avatar', 'world_pilot', 'urban_guy',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'solo_dj', 'luxury_lover', 'mind_maker',
        ],
        dataTrackingId: '',
      },
    ],
  },
  {
    questions: 'Alter',
    bgImage: imageRiskQuestion10.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        lifeStyle: [
          'online_avatar', 'hip_and_hopper', 'luxury_lover',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft zu',
        lifeStyle: [
          'solo_dj', 'yogi_guru', 'visionary',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Mal so mal so',
        lifeStyle: [
          'world_pilot', 'autopilot', 'multitasker',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        lifeStyle: [
          'freedom_mind', 'sun_herdsman', 'urban_guy',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft nicht zu',
        lifeStyle: [
          'mind_maker', 'social_politican', 'le_hipster',
        ],
        dataTrackingId: '',
      },
      {
        answer: 'Trifft gar nicht zu',
        lifeStyle: [
          'gatekeeper', 'storyteller', 'silver_athlete',
        ],
        dataTrackingId: '',
      },
    ],
  },
];

export default RiskQuestions;
