import axios from 'axios';
import { Lifestyle } from '../../types/risk/lifestyle';
import config from '../../config';

export const submitLifeStyles = async (
  lifeStyles: (string[] | string)[], consent: 0 | 1,
): Promise<Lifestyle[]> => (
  axios({
    method: 'POST',
    url: `${config().risk.apiUrl}/submit-lifestyles/${consent}`,
    data: lifeStyles,
  }).then((r) => r.data as Lifestyle[])
);
