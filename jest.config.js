module.exports = {
  setupFilesAfterEnv: ['<rootDir>/jest.setup.ts', 'jest-canvas-mock'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  collectCoverage: true,
  coverageReporters: ['lcov', 'text', 'text-summary'],
  moduleNameMapper: {
    '^.+\\.(css|ttf|woff|woff2)$': 'identity-obj-proxy',
    '^.+\\.(png|jpg|jpeg|svg|gif|avif)$': '<rootDir>/__mocks__/imageMock.ts',
  },
};
