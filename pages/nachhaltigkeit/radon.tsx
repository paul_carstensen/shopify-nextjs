import React, { useCallback, useEffect, useState, ReactElement } from 'react';
import { NextPage, GetServerSideProps, GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import Cookies from 'js-cookie';
import getAbsoluteUrl from '../../helpers/getAbsoluteUrl';
import { getPolicyText } from '../../helpers/nachhaltigkeit/consent_tracker/api';
import { getErrorMessage } from '../../helpers/nachhaltigkeit/radon/errorMessages';
import { ArabeskResultsPage } from '../../components/nachhaltigkeit/radon/ArabeskResultsPage';
import { postAddressData, postArabeskData } from '../../helpers/nachhaltigkeit/radon/api';
import { LandingPage } from '../../components/nachhaltigkeit/radon/LandingPage';
import { ArabeskPage } from '../../components/nachhaltigkeit/radon/ArabeskPage';
import { RadonResultsPage } from '../../components/nachhaltigkeit/radon/RadonResultsPage';
import { AddressData, ArabeskData } from '../../types/nachhaltigkeit/radon';
import { useApiErrors } from '../../context/errors/apiErrorsProvider';
import cache, { keys } from '../../cache';
import socialMediaImage from '../../assets/images/radon/result-split-view.jpg';
import ErrorPage500 from '../500';

const setFormSubmittedCookie = () => Cookies.set('cc_radon', '1', { expires: 90 });

type RadonPageProps = {
  socialMediaImageUrl: string;
  policyTextHtml: string;
}

const RadonPage: NextPage<RadonPageProps> = ({ socialMediaImageUrl, policyTextHtml }) => {
  const [addressData, setAddressData] = useState<AddressData | undefined>(undefined);
  const [radonLevel, setRadonLevel] = useState<number | undefined>(undefined);
  const [arabeskData, setArabeskData] = useState<ArabeskData | undefined>(undefined);
  const [page, setPage] = useState<'landing' | 'radonResults' | 'arabesk' | 'arabeskResults'>('landing');
  const [addressFormIsFocused, setAddressFormIsFocused] = useState(false);
  const { setErrors } = useApiErrors();

  useEffect(() => window.scrollTo(0, 0), [page]);

  const onAddressFormSubmit = useCallback(async (addressValues: AddressData) => {
    try {
      const { data: { radonLevel: radon } } = await postAddressData(addressValues);
      setRadonLevel(radon);
      setAddressData(addressValues);
      setFormSubmittedCookie();
      setPage('radonResults');
      setErrors('radon', '');
    } catch (e) {
      setErrors('radon', getErrorMessage(e));
    }
  }, [setErrors]);

  const onArabeskFormSubmit = useCallback(async (arabeskValues: ArabeskData) => {
    try {
      if (addressData) {
        await postArabeskData(addressData, arabeskValues);
      }
      setArabeskData(arabeskValues);
      setPage('arabeskResults');
      setErrors('arabesk', '');
    } catch (e) {
      setErrors('arabesk', getErrorMessage(e));
    }
  }, [addressData, setErrors]);

  const renderPage = (pageName: string): ReactElement => {
    switch (pageName) {
      case 'landing':
        return (
          <LandingPage
            onSplitViewCtaClick={() => setAddressFormIsFocused(true)}
            clearFocus={() => setAddressFormIsFocused(false)}
            addressFormIsFocused={addressFormIsFocused}
            onAddressFormSubmit={onAddressFormSubmit}
            policyTextHtml={policyTextHtml}
          />
        );
      case 'radonResults':
        if (typeof radonLevel === 'undefined') {
          return <ErrorPage500 />;
        }

        return (
          <RadonResultsPage
            radonLevel={radonLevel}
            onSplitViewCtaClick={() => setPage('arabesk')}
          />
        );
      case 'arabesk':
        return (
          <ArabeskPage
            onSubmit={onArabeskFormSubmit}
          />
        );
      case 'arabeskResults':
        if (typeof arabeskData === 'undefined') {
          return <ErrorPage500 />;
        }

        return (
          <ArabeskResultsPage
            arabeskWaterFilter={arabeskData.arabeskWaterFilter}
            arabeskWaterFilterMaintenance={arabeskData.arabeskWaterFilterMaintenance}
            arabeskWaterRarelyUsedSources={arabeskData.arabeskWaterRarelyUsedSources}
            arabeskWaterSiliconeGroutOk={arabeskData.arabeskWaterSiliconeGroutOk}
            arabeskSmokeAlarms={arabeskData.arabeskSmokeAlarms}
            arabeskCircuitBreakers={arabeskData.arabeskCircuitBreakers}
          />
        );
      default:
        return <ErrorPage500 />;
    }
  };

  const title = 'Radioaktives Radon in Deutschland';
  const description = 'Radioaktives Radon in Deutschland: Wie hoch ist die Gefahr in Deiner Wohngegend? Jetzt prüfen!';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />

        <meta key="og:title" property="og:title" content={title} />
        <meta key="og:description" property="og:description" content={description} />
        <meta key="og:image" property="og:image" content={socialMediaImageUrl} />
      </Head>

      {renderPage(page)}
    </>
  );
};

const getPolicyTextHtml = async (): Promise<string> => {
  let policyTextHtml: string | undefined = cache.get(keys.RADON_POLICY_TEXT);

  if (!policyTextHtml) {
    policyTextHtml = await getPolicyText('gimmick');

    cache.set(keys.RADON_POLICY_TEXT, policyTextHtml, 3600);
  }

  return policyTextHtml;
};

export const getServerSideProps: GetServerSideProps = async ({ req }: GetServerSidePropsContext) => ({
  props: {
    socialMediaImageUrl: getAbsoluteUrl(req, socialMediaImage.src).toString(),
    policyTextHtml: await getPolicyTextHtml(),
  },
});

export default RadonPage;
