import React, { useState, useEffect } from 'react';
import { NextPage, GetServerSideProps, GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import styled from 'styled-components';
import { Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import { QuestionCTA } from '../../components/QuestionCTA';
import { KeepMeUpdatedBody, Lifestyle, QuestionRisk } from '../../types/risk/lifestyle';
import riskQuestions from '../../helpers/lifeStyle/riskQuestions';
import riskQuestionStart from '../../assets/images/risk/riskQuestion_start.jpg';
import { submitLifeStyles } from '../../helpers/lifeStyle/api';
import { PickYourLifestyle } from '../../components/Risk/Lifestyle/PickYourLifestyle';
import getAbsoluteUrl from '../../helpers/getAbsoluteUrl';

const SliderContainer = styled.div`
  margin-bottom: 30px;
  max-width: 300px;
  margin: 0 auto 30px auto;

  @media screen and (min-width: 768px) {
    min-height: 700px;
    display: flex;
    align-items: center;
  }
`;

const Container = styled.div`
  margin: auto;
  max-width: 950px;
  margin-bottom: 40px;
`;

const QuestionContainer = styled.div`
  max-width: 400px;
  margin: auto;
`;

const userRiskData = {
  email: null,
  submittedQuestions: [],
  selectedLifestyleKey: '',
};

const CONSENT_GTM_NAME = 'Google Analytics';

const usercentricsLoaded = (): boolean => (
  // Usercentrics SDK will not be loaded during SSR or in development mode
  typeof window?.UC_UI?.getServicesBaseInfo === 'function'
);

type ConsentObject = {
  name: string;
  consent: {
    status: boolean;
  };
  id: string;
}

const getConsentObject = (vendorName: string): ConsentObject | undefined => {
  if (!usercentricsLoaded()) {
    return undefined;
  }

  const consents = window.UC_UI.getServicesBaseInfo() as ConsentObject[];

  return consents.find(({ name }) => name === vendorName);
};

const getConsentStatus = (vendorName: string): boolean => {
  const { consent } = getConsentObject(vendorName) || { consent: { status: false } };

  return consent.status;
};

type LifestylePageProps = {
  socialMediaImageUrl: string;
}

const LifestylePage: NextPage<LifestylePageProps> = ({ socialMediaImageUrl }) => {
  const title = 'text title head';
  const description = 'text description head';

  const [startQuestions, setStartQuestions] = useState<boolean>(false);
  const [riskData, setRiskData] = useState<KeepMeUpdatedBody>(userRiskData);
  const [hasConsent, setHasConsent] = useState(false);
  const [top3Lifestyles, setTop3Lifestyles] = useState<Lifestyle[]>([]);

  const handleUserCentricsInitialized = () => {
    setHasConsent(getConsentStatus(CONSENT_GTM_NAME));
  };

  useEffect(() => {
    window?.addEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);

    return () => {
      window?.removeEventListener('UC_UI_INITIALIZED', handleUserCentricsInitialized);
    };
  }, []);

  const answerQuestion = async (question: string, answer: string[], i: number, totalRiskQuestions: number) => {
    const newAnswer: QuestionRisk = {
      question,
      answer,
    };

    const copyRiskData: KeepMeUpdatedBody = JSON.parse(JSON.stringify(riskData));
    copyRiskData.submittedQuestions.push(newAnswer);
    localStorage.setItem('riskData', JSON.stringify(copyRiskData));

    setRiskData(copyRiskData);

    if (i + 1 === totalRiskQuestions) {
      const userConsent = hasConsent ? 1 : 0;
      const lifestyleAnswers = copyRiskData.submittedQuestions.map((x) => x.answer);
      const lifeStyles = await submitLifeStyles(lifestyleAnswers, userConsent);
      setTop3Lifestyles(lifeStyles);
    }
  };

  const buildHead = () => (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />

      <meta key="og:title" property="og:title" content={title} />
      <meta key="og:description" property="og:description" content={description} />
      <meta key="og:image" property="og:image" content={socialMediaImageUrl} />
    </Head>
  );

  if (top3Lifestyles.length === 0) {
    return (
      <>
        {buildHead()}
        <Container>
          <QuestionContainer>
            <SliderContainer>
              {
                !startQuestions ? (
                  <QuestionCTA
                    key="questionKey"
                    title="Candy jelly pie gummies cotton candy chocolate bar fruitcake."
                    options={[
                      { title: 'Let\'s do!', dataTrackingId: 'dataTrackingId', onClick: () => setStartQuestions(true) }]}
                    minHeight="calc(100vh - 140px)"
                    bgImage={riskQuestionStart.src}
                    center
                    btnFontSizeLarge={18}
                    minHeightDesktop="500px"
                  />
                ) : (
                  <Slider
                    visibleSlidesBreakpoints={1}
                    enableArrows={false}
                    enableMouseWheel={false}
                    disableHorizontalScroll
                  >
                    {riskQuestions.map((riskQuestion, i) => (
                      <QuestionCTA
                        key={`question-${riskQuestion.questions}`}
                        title={riskQuestion.questions}
                        options={riskQuestion.answers.map((option) => ({
                          title: option.answer,
                          dataTrackingId: option.dataTrackingId,
                          lifeStyle: option.lifeStyle,
                          onClick: () => answerQuestion(riskQuestion.questions, option.lifeStyle, i, riskQuestions.length),
                        }))}
                        minHeight="calc(100vh - 140px)"
                        minHeightDesktop="500px"
                        pagination={i + 1}
                        totalPagination={riskQuestions.length}
                        bgImage={riskQuestion.bgImage}
                      />
                    ))}
                  </Slider>
                )
              }
            </SliderContainer>
          </QuestionContainer>
        </Container>
      </>
    );
  }

  return (
    <>
      {buildHead()}
      <Container>
        <PickYourLifestyle
          pageTitle="Pick your lifestyle"
          pageDescription="Here are suggested 3 lifestyles based on your answers"
          lifestyles={top3Lifestyles}
        />
      </Container>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }: GetServerSidePropsContext) => ({
  props: {
    socialMediaImageUrl: getAbsoluteUrl(req, riskQuestionStart.src).toString(),
  },
});

export default LifestylePage;
